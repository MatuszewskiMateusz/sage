package com.sagepay.exercises;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class Exercises {

    /**
     * Find the first non-repeatable character in a string.
     * Constraints:
     * - The string contains characters in range: [A-Z][a-z]
     *
     * @param input the given string
     * @return the first non-repeatable character of the input string
     */
    public Optional<Character> getFirstNonRepeatableChar(String input) {
        Map<Character, Integer> occurenceMap = new LinkedHashMap<>();
        int newCount;
        Integer currentCount;
        /**
         * Loop over the String and populate linked hashmap with counts for each character
         */
        for (int i=0; i< input.length(); i++) {
            char ch = input.charAt(i);
            currentCount = occurenceMap.get(ch);
            if (currentCount == null) {
                newCount = 1;
            } else {
                newCount = currentCount + 1;
            }
            occurenceMap.put(ch, newCount);
        }
        /**
         * Find first element in the map with count==1
         */
        for (Map.Entry<Character, Integer> entry : occurenceMap.entrySet()) {
            if (entry.getValue().equals(1)) {
                return Optional.of(entry.getKey());
            }
        }
        return Optional.empty();
    }

    /**
     * Counts the number of words in the largest sentence of a string.
     * Constraints:
     * - Each sentence ends with one of: '.' '!' '?'.
     * - Each word ends with a space.
     *
     * @param input the given input
     * @return the size of the largest sentence
     */
    public Integer countWordsInLargestSentence(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }
        /**
         * Inefficient, but really concise implementation.
         *
         * Split String on ".", "!" and "?". we get an array of Strings which contains each sentence.
         * Trim to remove leading spaces and again split - this time on space (" ").
         * Again we get an array of Strings, this time containing single words check size of such array and compare to max.
         */
        String[] sentences = input.split("\\.|!|\\?");
        int maxLength = -1;
        int len;
        for (String s : sentences) {
            len = s.trim().split(" ").length;
            maxLength = maxLength > len ? maxLength : len;
        }
        return maxLength;
    }



    /**
     * Given the class Tree, count the nodes of the tree that are visible.
     * - The root of the tree is always visible.
     * - An internal node is visible if its value is greater than or equal to the root and there is no other value
     *     in the path to the root greater than the value of the node.
     * - The values in the nodes are always greater than or equal to 0
     *
     * @param tree the given tree
     * @return int the number of visible nodes.
     */
    public Integer numberOfVisibleNodes(Tree tree) {
        if (tree!= null && tree.value != null) {
            int counter = 1; // the head always counts
            counter += numVisibleNodesInSubtree(tree.left, tree.value);
            counter += numVisibleNodesInSubtree(tree.right, tree.value);
            return counter;
        } else {
            return 0;
        }
    }

    /**
     * Recursively calculates number of visible nodes in a subtree
     *
     * @param tree subtree in which we need to calculate number of visible nodes
     * @param min minimum value for a node to be considered visible
     * @return number of visible nodes in a subtree
     */
    private int numVisibleNodesInSubtree(Tree tree, int min) {
        /**
         * Check if the given subtree and the subtree-head-value is not null
         */
        if (tree == null || tree.value == null) {
            return 0;
        }

        int left=0, right=0;
        boolean isVisible = tree.value >= min;

        /**
         * Calculate the new minimum value for the node to be considered visible as
         * the max(givenMin, tree.value)
         */
        int newMin = min > tree.value ? min : tree.value;
        /**
         * Recursively descend left and right sub-trees
         */
        if (tree.left != null) {
            left = numVisibleNodesInSubtree(tree.left, newMin);
        }
        if (tree.right != null) {
            right = numVisibleNodesInSubtree(tree.right, newMin);
        }
        /**
         * The return value is the sum of numbers of visible nodes in each sub-tree
         * incremented by one if current node is visible as well
         */
        return left + right + (isVisible ? 1 : 0);
    }

    public static class Tree {
        public Integer value;
        public Tree left;
        public Tree right;

        public Tree(int value, Tree left, Tree right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }
}
