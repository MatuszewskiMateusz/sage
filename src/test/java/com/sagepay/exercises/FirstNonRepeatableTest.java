package com.sagepay.exercises;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class FirstNonRepeatableTest {

    private Exercises exercises;

    @Before
    public void setUp() {
        this.exercises = new Exercises();
    }

    @Test
    public void test_WhenInputIsEmpty_ShouldReturnNull() {
        // Given
        String input = "";

        // When
        Optional<Character> actual = exercises.getFirstNonRepeatableChar(input);

        // Then
        assertFalse(actual.isPresent());
    }

    @Test
    public void test_WhenAllCharsAreDuplicate_ShouldReturnNull() {
        // Given
        String input = "aa";

        // When
        Optional<Character> actual = exercises.getFirstNonRepeatableChar(input);

        // Then
        assertFalse(actual.isPresent());
    }

    @Test
    public void test_WhenCharsAreInDifferentCase_ShouldReturnTheFirst() {
        // Given
        String input = "aA";
        Character expected = 'a';

        // When
        Optional<Character> actual = exercises.getFirstNonRepeatableChar(input);

        // Then
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    public void test_WhenOnlyOneCharIsNotDuplicate_ShouldReturnThat() {
        // Given
        String input = "abcdefghijklmnopqrstuvwxyzAAabcdefghijklmnopqrstuvwxy";
        Character expected = 'z';

        // When
        Optional<Character> actual = exercises.getFirstNonRepeatableChar(input);

        // Then
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}