package com.sagepay.exercises;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumberOfWordsInLargestSentenceTest {

    private Exercises exercises;

    @Before
    public void setUp() {
        this.exercises = new Exercises();
    }


    @Test
    public void testCount_WithEmptyInput_ShouldReturnZero() {
        // Given
        String input = "";
        Integer expected = 0;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_OneSentence_ShouldReturnCorrectLength() {
        // Given
        String input = "This input has one sentence with 8 words.";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_TwoSentencesWithSameLength_ShouldReturnCorrectLength() {
        // Given
        String input = "This input has two sentences with 8 words. This input has two sentences with 8 words.";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_TwoSentencesWithSameLengthSeparatedWithExclMark_ShouldReturnCorrectLength() {
        // Given
        String input = "This sentence has 5 words! This sentence has more, it has 8 words.";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_TwoSentencesWithSameLengthSeparatedWithQuestMark_ShouldReturnCorrectLength() {
        // Given
        String input = "This sentence has 5 words? This sentence has more, it has 8 words.";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_TwoSentencesWithMissingDelimiterInTheEnd_ShouldReturnCorrectLength() {
        // Given
        String input = "This sentence has 5 words. This sentence has more, it has 8 words";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void testCount_TwoSentencesWithMultipleDelimiters_ShouldReturnCorrectLength() {
        // Given
        String input = "This sentence has 5 words... This sentence has more, it has 8 words";
        Integer expected = 8;

        // When
        Integer actual = exercises.countWordsInLargestSentence(input);

        // Then
        assertEquals(expected, actual);
    }
}
