package com.sagepay.exercises;

import org.junit.Before;
import org.junit.Test;

import static com.sagepay.exercises.Exercises.*;
import static org.junit.Assert.assertEquals;

public class TreeVisibleNodesTest {

    private Exercises exercises;

    @Before
    public void setUp() {
        this.exercises = new Exercises();
    }

    @Test
    public void test_EmptyTree_ShouldReturnZero() {
        // Given
        Tree t = null;
        Integer expected = 0;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithOnlyRootNode_ShouldReturnOne() {
        // Given
        Tree t = new Tree(1, null, null);
        Integer expected = 1;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithOnlyLeftSubtreeWithGreaterValue_ShouldReturnCorrectResult() {
        // Given
        //      1
        //   2     null
        Tree left = new Tree(2, null, null);
        Tree t = new Tree(1, left, null);
        Integer expected = 2;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithOnlyLeftSubtreeWithEqualValue_ShouldReturnCorrectResult() {
        // Given
        //      2
        //   2     null
        Tree left = new Tree(2, null, null);
        Tree t = new Tree(2, left, null);
        Integer expected = 2;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithNestedLeftSubtreeWithGreaterValue_ShouldReturnCorrectResult() {
        // Given
        //        1
        //     2     null
        //   3   3
        Tree subLeft = new Tree(3, null, null);
        Tree subRight = new Tree(3, null, null);
        Tree left = new Tree(2, subLeft, subRight);
        Tree t = new Tree(1, left, null);

        Integer expected = 4;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithNestedLeftSubtreeWithEqualValue_ShouldReturnCorrectResult() {
        // Given
        //        1
        //     3     null
        //   3   2
        Tree subLeft = new Tree(2, null, null);
        Tree subRight = new Tree(3, null, null);
        Tree left = new Tree(3, subLeft, subRight);
        Tree t = new Tree(1, left, null);

        Integer expected = 3;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }

    @Test
    public void test_TreeWithGreatestValueInRoot_ShouldReturnOne() {
        // Given
        //        10
        //     3     null
        //   3   2
        Tree subLeft = new Tree(3, null, null);
        Tree subRight = new Tree(3, null, null);
        Tree left = new Tree(3, subLeft, subRight);
        Tree t = new Tree(10, left, null);

        Integer expected = 1;

        // When
        Integer actual = exercises.numberOfVisibleNodes(t);

        // Then
        assertEquals(expected, actual);
    }
}